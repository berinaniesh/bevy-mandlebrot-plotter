use num_complex::Complex;

const MAX_ITERATIONS: u16 = 256;

const IMAGE_WIDTH: u32 = 1280;
const IMAGE_HEIGHT: u32 = 1280;

fn main() {
    let cx_min = -2f64;
    let cx_max = 1f64;
    let cy_min = -1.5f64;
    let cy_max = 1.5f64;

    let scale_x = (cx_max - cx_min) / IMAGE_WIDTH as f64;
    let scale_y = (cy_max - cy_min) / IMAGE_HEIGHT as f64;

    let mut imagebuffer = image::ImageBuffer::new(IMAGE_WIDTH, IMAGE_HEIGHT);

    for (x, y, pixel) in imagebuffer.enumerate_pixels_mut() {
        
        let cx = cx_min + x as f64 * scale_x;
        let cy = cy_min + y as f64 * scale_y;

        let c = Complex::new(cx, cy);
        let mut z = Complex::new(0f64, 0f64);

        let mut i = 0;

        for t in 0..MAX_ITERATIONS {
            if z.norm() > 2.0 {break}
            z = z*z+c;
            i = t;
        }
        *pixel = image::Luma([i as u8]);
    }
    imagebuffer.save("fractal.png").unwrap();
}
